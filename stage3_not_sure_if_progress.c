
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef int bool;
#define true 1
#define false 0

typedef struct {
	char decision;
	char snake;
	short caption;
	int grip;
	unsigned char support;
	long apple;
	char dog;
	int haircut;
	int representative;
	char authority;
	unsigned short plot;
	float pig;
	double basket;
	int laborer;
	double net;
	char parent[7];
} myStruct;

static int compareptrs(const void *p1, const void *p2)
{      
	printf("comparing...\n");
        myStruct *ptr1 = *(myStruct **) p1;
        myStruct *ptr2 = *(myStruct **) p2;
        
        //desc
        if(ptr1->parent < ptr2->parent) {
                return 1;
        }
        if(ptr1->parent > ptr2->parent) {
                return -1;                      
        }

        //desc
        if(ptr1->haircut < ptr2->haircut) {
                return 1;
        }
        if(ptr1->haircut > ptr2->haircut) {
                return -1;                      
        }    

        //asc
        if(ptr1->laborer > ptr2->laborer) {
                return 1;
        }
        if(ptr1->laborer < ptr2->laborer) {
                return -1;                      
        }    

        //desc
        if(ptr1->decision < ptr2->decision) {
                return 1;
        }
        if(ptr1->decision > ptr2->decision) {
                return -1;                      
        }    

        //asc
        if(ptr1->plot > ptr2->plot) {
                return 1;
        }
        if(ptr1->plot < ptr2->plot) {
                return -1;                      
        }     

        //desc
        if(ptr1->caption < ptr2->caption) {
                return 1;
        }
        if(ptr1->caption > ptr2->caption) {
                return -1;                      
        }    

        //desc
        if(ptr1->snake < ptr2->snake) {
                return 1;
        }
        if(ptr1->snake > ptr2->snake) {
                return -1;                      
        }    

        //asc
        if(ptr1->pig > ptr2->pig) {
                return 1;
        }
        if(ptr1->pig < ptr2->pig) {
                return -1;                      
        } 

        //asc
        if(ptr1->authority > ptr2->authority) {
                return 1;
        }
        if(ptr1->authority < ptr2->authority) {
                return -1;                      
        }   

        //asc
        if(ptr1->apple > ptr2->apple) {
                return 1;
        }
        if(ptr1->apple < ptr2->apple) {
                return -1;                      
        }   

        //asc
        if(ptr1->dog > ptr2->dog) {
                return 1;
        }
        if(ptr1->dog < ptr2->dog) {
                return -1;                      
        }   

        //desc
        if(ptr1->basket < ptr2->basket) {
                return 1;
        }
        if(ptr1->basket > ptr2->basket) {
                return -1;                      
        }  

        //asc
        if(ptr1->support > ptr2->support) {
                return 1;
        }
        if(ptr1->support < ptr2->support) {
                return -1;                      
        }   

        //desc
        if(ptr1->representative < ptr2->representative) {
                return 1;
        }
        if(ptr1->representative > ptr2->representative) {
                return -1;                      
        }

        //asc
        if(ptr1->net > ptr2->net) {
                return 1;
        }
        if(ptr1->net < ptr2->net) {
                return -1;                      
        }   

        //desc
        if(ptr1->grip < ptr2->grip) {
                return 1;
        }
        if(ptr1->grip > ptr2->grip) {
                return -1;                      
        }

	printf("end of compare function, maybe error? returning 0");
	return 0;
                                                        
}

int main (int argc, char *argv[]) {
	if (argc < 3){
		fprintf(stderr, "Please enter input and output filename as arguments!\n");
		exit(2);
	}
	
	FILE* inputFile = fopen(argv[1], "rb");
	FILE* outputFile = fopen(argv[2], "wb");     
	
	myStruct Struct;
	myStruct *Structptr;
	myStruct **arrayOfStruct;
		if ( inputFile == NULL ) {
			fprintf(stderr, "Unable to open input file!\n");                                                                                                             
		        exit(1);   
		}
		
		printf("decision, snake, caption, grip, support, apple, dog, haircut, representative, authority, plot, pig, basket, laborer, net, parent\n");
		
		int size = 1;
		int count = 0;
		*arrayOfStruct = malloc(size * sizeof(Struct));
		if (*arrayOfStruct == 0){
			printf("ERROR: Out of memory: arrayOfStruct\n");
			return 1;
		}
		
		while(!feof(inputFile)) {
			printf("size of array = %ld\n size of struct = %ld\n size variable = %d\n", sizeof(*arrayOfStruct), sizeof(Struct), size);
			if(sizeof(*arrayOfStruct) < size*sizeof(Struct)) {
				printf("reallocating memory from %ld     to %ld ?\n", sizeof(*arrayOfStruct), size*sizeof(Struct));
				*arrayOfStruct = realloc(*arrayOfStruct, size*sizeof(Struct));
				//if (tmp == NULL) {
				//	printf("tmp is Null");
				//Out of memory, but the array still exists
				//}
				//else{
				//	*arrayOfStruct = (myStruct *)tmp;
				//}                                                                                 
				printf("SIZE IS NOW %ld\n", sizeof(*arrayOfStruct));
			}
			
			if (fread( &arrayOfStruct[count]->decision, sizeof(arrayOfStruct[count]->decision), 1, inputFile) != 0){
                        	printf("%hhd,", arrayOfStruct[count]->decision);
                        }
                        else {
                        	break;
                        }
                        fread( &arrayOfStruct[count]->snake, sizeof(arrayOfStruct[count]->snake), 1, inputFile);
                        printf(" %c,", arrayOfStruct[count]->snake);
                        fread( &arrayOfStruct[count]->caption, sizeof(arrayOfStruct[count]->caption), 1, inputFile);
                        printf(" %hd,", arrayOfStruct[count]->caption);
                        fread( &arrayOfStruct[count]->grip, sizeof(arrayOfStruct[count]->grip), 1, inputFile);
                        printf(" %d,", arrayOfStruct[count]->grip);
                        fread( &arrayOfStruct[count]->support, sizeof(arrayOfStruct[count]->support), 1, inputFile);
                        printf(" %hhu,", arrayOfStruct[count]->support);
                        fread( &arrayOfStruct[count]->apple, sizeof(arrayOfStruct[count]->apple), 1, inputFile);
                        printf(" %ld,", arrayOfStruct[count]->apple);
                        fread( &arrayOfStruct[count]->dog, sizeof(arrayOfStruct[count]->dog), 1, inputFile);
                        printf(" %hhx,", arrayOfStruct[count]->dog);
                        fread( &arrayOfStruct[count]->haircut, sizeof(arrayOfStruct[count]->haircut), 1, inputFile);
                        printf(" %d,", arrayOfStruct[count]->haircut);
                        fread( &arrayOfStruct[count]->representative, sizeof(arrayOfStruct[count]->representative), 1, inputFile);
                        printf(" %d,", arrayOfStruct[count]->representative);
                        fread( &arrayOfStruct[count]->authority, sizeof(arrayOfStruct[count]->authority), 1, inputFile);
                        printf(" %hhd,", arrayOfStruct[count]->authority);
                        fread( &arrayOfStruct[count]->plot, sizeof(arrayOfStruct[count]->plot), 1, inputFile);
                        printf(" %hu,", arrayOfStruct[count]->plot);
                        fread( &arrayOfStruct[count]->pig, sizeof(arrayOfStruct[count]->pig), 1, inputFile);
                        printf(" %f,", arrayOfStruct[count]->pig);
                        fread( &arrayOfStruct[count]->basket, sizeof(arrayOfStruct[count]->basket), 1, inputFile);
                        printf(" %lf,", arrayOfStruct[count]->basket);
                        fread( &arrayOfStruct[count]->laborer, sizeof(arrayOfStruct[count]->laborer), 1, inputFile);
                        printf(" %d,", arrayOfStruct[count]->laborer);
                        fread( &arrayOfStruct[count]->net, sizeof(arrayOfStruct[count]->net), 1, inputFile);
                        printf(" %lf,", arrayOfStruct[count]->net);
                        fread( &arrayOfStruct[count]->parent, sizeof(arrayOfStruct[count]->parent), 1, inputFile);
                        printf(" %s\n", arrayOfStruct[count]->parent);
                        size++;
                        count++;
			printf("next loop\n");	
		}
		fclose(inputFile);

		printf(" closed input file\n");

    		if (outputFile == NULL) {
			printf("Cannot open filename\n");
			exit(1);
    		}

		qsort(arrayOfStruct, count, sizeof(myStruct *), compareptrs);
		
		printf("sort complete");

		int loopCount = 0;

		for(int i=0; i< count; i++) {
                        if(fwrite(&arrayOfStruct[i]->decision, sizeof(arrayOfStruct[i]->decision), 1, outputFile)!=0) {
                        	printf(" hit first write statement \n");
			}
                        else{
                        	printf(" breaking \n");
				break;
			}
                        fwrite(&arrayOfStruct[i]->snake, sizeof(arrayOfStruct[i]->snake), 1, outputFile);
                        fwrite(&arrayOfStruct[i]->caption, sizeof(arrayOfStruct[i]->caption), 1, outputFile);
                        fwrite(&arrayOfStruct[i]->grip, sizeof(arrayOfStruct[i]->grip), 1, outputFile);
                        fwrite(&arrayOfStruct[i]->support, sizeof(arrayOfStruct[i]->support), 1, outputFile);
                        fwrite(&arrayOfStruct[i]->apple, sizeof(arrayOfStruct[i]->apple), 1, outputFile);                                                                                           
                        fwrite(&arrayOfStruct[i]->dog, sizeof(arrayOfStruct[i]->dog), 1, outputFile);
                        fwrite(&arrayOfStruct[i]->haircut, sizeof(arrayOfStruct[i]->haircut), 1, outputFile);
                        fwrite(&arrayOfStruct[i]->representative, sizeof(arrayOfStruct[i]->representative), 1, outputFile);
                        fwrite(&arrayOfStruct[i]->authority, sizeof(arrayOfStruct[i]->authority), 1, outputFile);                                                                                   
                        fwrite(&arrayOfStruct[i]->plot, sizeof(arrayOfStruct[i]->plot), 1, outputFile);                                                                                             
                        fwrite(&arrayOfStruct[i]->pig, sizeof(arrayOfStruct[i]->pig), 1, outputFile);                                                                                               
                        fwrite(&arrayOfStruct[i]->basket, sizeof(arrayOfStruct[i]->basket), 1, outputFile);                                                                                         
                        fwrite(&arrayOfStruct[i]->laborer, sizeof(arrayOfStruct[i]->laborer), 1, outputFile);                                                                                       
                        fwrite(&arrayOfStruct[i]->net, sizeof(arrayOfStruct[i]->net), 1, outputFile);                                                                                               
                        fwrite(&arrayOfStruct[i]->parent, sizeof(arrayOfStruct[i]->parent), 1, outputFile);                                                                                         
			printf("loop  %d of write\n", loopCount);
			loopCount++;
		}

  		fwrite(&arrayOfStruct, sizeof(Struct), count, outputFile);
		free(arrayOfStruct);
  		fclose(outputFile);
		printf("done");
	return 0;
}
