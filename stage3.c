
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef int bool;
#define true 1
#define false 0

typedef struct {
	char decision;
	char snake;
	short caption;
	int grip;
	unsigned char support;
	long apple;
	char dog;
	int haircut;
	int representative;
	char authority;
	unsigned short plot;
	float pig;
	double basket;
	int laborer;
	double net;
	char parent[7];
} myStruct;

static int compareptrs(const void *p1, const void *p2)
{      
	printf("comparing...\n");
        myStruct *ptr1 = *(myStruct **) p1;
        myStruct *ptr2 = *(myStruct **) p2;
        
        //desc
        if(ptr1->parent < ptr2->parent) {
                return 1;
        }
        if(ptr1->parent > ptr2->parent) {
                return -1;                      
        }

        //desc
        if(ptr1->haircut < ptr2->haircut) {
                return 1;
        }
        if(ptr1->haircut > ptr2->haircut) {
                return -1;                      
        }    

        //asc
        if(ptr1->laborer > ptr2->laborer) {
                return 1;
        }
        if(ptr1->laborer < ptr2->laborer) {
                return -1;                      
        }    

        //desc
        if(ptr1->decision < ptr2->decision) {
                return 1;
        }
        if(ptr1->decision > ptr2->decision) {
                return -1;                      
        }    

        //asc
        if(ptr1->plot > ptr2->plot) {
                return 1;
        }
        if(ptr1->plot < ptr2->plot) {
                return -1;                      
        }     

        //desc
        if(ptr1->caption < ptr2->caption) {
                return 1;
        }
        if(ptr1->caption > ptr2->caption) {
                return -1;                      
        }    

        //desc
        if(ptr1->snake < ptr2->snake) {
                return 1;
        }
        if(ptr1->snake > ptr2->snake) {
                return -1;                      
        }    

        //asc
        if(ptr1->pig > ptr2->pig) {
                return 1;
        }
        if(ptr1->pig < ptr2->pig) {
                return -1;                      
        } 

        //asc
        if(ptr1->authority > ptr2->authority) {
                return 1;
        }
        if(ptr1->authority < ptr2->authority) {
                return -1;                      
        }   

        //asc
        if(ptr1->apple > ptr2->apple) {
                return 1;
        }
        if(ptr1->apple < ptr2->apple) {
                return -1;                      
        }   

        //asc
        if(ptr1->dog > ptr2->dog) {
                return 1;
        }
        if(ptr1->dog < ptr2->dog) {
                return -1;                      
        }   

        //desc
        if(ptr1->basket < ptr2->basket) {
                return 1;
        }
        if(ptr1->basket > ptr2->basket) {
                return -1;                      
        }  

        //asc
        if(ptr1->support > ptr2->support) {
                return 1;
        }
        if(ptr1->support < ptr2->support) {
                return -1;                      
        }   

        //desc
        if(ptr1->representative < ptr2->representative) {
                return 1;
        }
        if(ptr1->representative > ptr2->representative) {
                return -1;                      
        }

        //asc
        if(ptr1->net > ptr2->net) {
                return 1;
        }
        if(ptr1->net < ptr2->net) {
                return -1;                      
        }   

        //desc
        if(ptr1->grip < ptr2->grip) {
                return 1;
        }
        if(ptr1->grip > ptr2->grip) {
                return -1;                      
        }

	printf("end of compare function, maybe error? returning 0");
	return 0;
                                                        
}

int main (int argc, char *argv[]) {
	if (argc < 3){
		fprintf(stderr, "Please enter input and output filename as arguments!\n");
		exit(2);
	}
	
	FILE* inputFile = fopen(argv[1], "rb");
	FILE* outputFile = fopen(argv[2], "wb");     
	
	myStruct Struct;
	myStruct *Structptr;
	myStruct **arrayOfStruct;
		if ( inputFile == NULL ) {
			fprintf(stderr, "Unable to open input file!\n");                                                                                                             
		        exit(1);   
		}
		
		printf("decision, snake, caption, grip, support, apple, dog, haircut, representative, authority, plot, pig, basket, laborer, net, parent\n");
		
		int size = 1;
		int count = 0;
		*arrayOfStruct = malloc(size*sizeof(Struct)); 

		while(!feof(inputFile)) {
			
			if(sizeof((*arrayOfStruct)) < size*sizeof(Structptr)) {
				printf("reallocating memory from %ld     to %ld ?\n", sizeof(*arrayOfStruct), size*sizeof(Structptr));
				void *tmp = realloc(*arrayOfStruct, size*sizeof(Struct));
				if (tmp == NULL) {
					printf("tmp is Null");
				//Out of memory, but the array still exists
				}
				else{
					*arrayOfStruct = (myStruct *)tmp;
				}                                                                                 
				printf("SIZE IS NOW %ld\n", sizeof(*arrayOfStruct));
			}
			if(fread(&Struct.decision, sizeof(Struct.decision), 1, inputFile)!= 0) {
                                printf("%hhd,", Struct.decision);
                        }
                        else{
                                break;
                        }
                        fread(&Struct.snake, sizeof(Struct.snake), 1, inputFile);
                        printf(" %c,", Struct.snake);
                        fread(&Struct.caption, sizeof(Struct.caption), 1, inputFile);
                        printf(" %hd,", Struct.caption);
                        fread(&Struct.grip, sizeof(Struct.grip), 1, inputFile);
                        printf(" %d,", Struct.grip);
                        fread(&Struct.support, sizeof(Struct.support), 1, inputFile);
                        printf(" %hhu,", Struct.support);
                        fread(&Struct.apple, sizeof(Struct.apple), 1, inputFile);
                        printf(" %ld,", Struct.apple);
                        fread(&Struct.dog, sizeof(Struct.dog), 1, inputFile);
                        printf(" %hhx,", Struct.dog);
                        fread(&Struct.haircut, sizeof(Struct.haircut), 1, inputFile);
                        printf(" %d,", Struct.haircut);
                        fread(&Struct.representative, sizeof(Struct.representative), 1, inputFile);
                        printf(" %d,", Struct.representative);
                        fread(&Struct.authority, sizeof(Struct.authority), 1, inputFile);
                        printf(" %hhd,", Struct.authority);
                        fread(&Struct.plot, sizeof(Struct.plot), 1, inputFile);
                        printf(" %hu,", Struct.plot);
                        fread(&Struct.pig, sizeof(Struct.pig), 1, inputFile);
                        printf(" %f,", Struct.pig);
                        fread(&Struct.basket, sizeof(Struct.basket), 1, inputFile);
                        printf(" %lf,", Struct.basket);
                        fread(&Struct.laborer, sizeof(Struct.laborer), 1, inputFile);
                        printf(" %d,", Struct.laborer);
                        fread(&Struct.net, sizeof(Struct.net), 1, inputFile);
                        printf(" %lf,", Struct.net);
                        fread(&Struct.parent, sizeof(Struct.parent), 1, inputFile);
                        printf(" %s\n", Struct.parent);
			
			printf("adding struct to array\n");
			(*arrayOfStruct)[count] = Struct;
                        size++;
                        count++;	
		}
		fclose(inputFile);

		printf(" closed input file\n");

    		if (outputFile == NULL) {
			printf("Cannot open filename\n");
			exit(1);
    		}

		qsort(arrayOfStruct, count, sizeof(Structptr), compareptrs);
		
		printf("sort complete");
		
		for(int i=0; i< sizeof(arrayOfStruct); i++) {
                        if(fwrite(&(arrayOfStruct[i]->decision), sizeof(Struct.decision), 1, outputFile)!=0) {
                        	printf(" debug\n");
			}
                        else{
                            printf(" debug1\n");
				break;
			}
			printf(" debug\n");
                        fwrite(&arrayOfStruct[i]->snake, sizeof(arrayOfStruct[i]->snake), 1, outputFile);
                        fwrite(&arrayOfStruct[i]->caption, sizeof(arrayOfStruct[i]->caption), 1, outputFile);
                        fwrite(&arrayOfStruct[i]->grip, sizeof(arrayOfStruct[i]->grip), 1, outputFile);
                        fwrite(&arrayOfStruct[i]->support, sizeof(arrayOfStruct[i]->support), 1, outputFile);
                        fwrite(&arrayOfStruct[i]->apple, sizeof(arrayOfStruct[i]->apple), 1, outputFile);                                                                                           
                        fwrite(&arrayOfStruct[i]->dog, sizeof(arrayOfStruct[i]->dog), 1, outputFile);
                        fwrite(&arrayOfStruct[i]->haircut, sizeof(arrayOfStruct[i]->haircut), 1, outputFile);
                        fwrite(&arrayOfStruct[i]->representative, sizeof(arrayOfStruct[i]->representative), 1, outputFile);
                        fwrite(&arrayOfStruct[i]->authority, sizeof(arrayOfStruct[i]->authority), 1, outputFile);                                                                                   
                        fwrite(&arrayOfStruct[i]->plot, sizeof(arrayOfStruct[i]->plot), 1, outputFile);                                                                                             
                        fwrite(&arrayOfStruct[i]->pig, sizeof(arrayOfStruct[i]->pig), 1, outputFile);                                                                                               
                        fwrite(&arrayOfStruct[i]->basket, sizeof(arrayOfStruct[i]->basket), 1, outputFile);                                                                                         
                        fwrite(&arrayOfStruct[i]->laborer, sizeof(arrayOfStruct[i]->laborer), 1, outputFile);                                                                                       
                        fwrite(&arrayOfStruct[i]->net, sizeof(arrayOfStruct[i]->net), 1, outputFile);                                                                                               
                        fwrite(&arrayOfStruct[i]->parent, sizeof(arrayOfStruct[i]->parent), 1, outputFile);                                                                                         
			
		}

  		fwrite(&arrayOfStruct, sizeof(Struct), count, outputFile);
		free(arrayOfStruct);
  		fclose(outputFile);
		printf("done");
	return 0;
}
