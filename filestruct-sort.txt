
Sorting order (in order of precedence):
 
parent in descending order. 
haircut in descending order. 
laborer in ascending order. 
decision in descending order. 
plot in ascending order. 
caption in descending order. 
snake in descending order. 
pig in ascending order. 
authority in ascending order. 
apple in ascending order. 
dog in ascending order. 
basket in descending order. 
support in ascending order. 
representative in descending order. 
net in ascending order. 
grip in descending order. 
