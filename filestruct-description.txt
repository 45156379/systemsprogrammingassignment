Records of your data files contain the following fields in the following sequence.
  
The field called 'decision' is a 1-byte Boolean
The field called 'snake' is a 1-byte character
The field called 'caption' is a 2-byte integer
The field called 'grip' is a 4-byte Boolean
The field called 'support' is a 1-byte unsigned integer
The field called 'apple' is an 8-byte integer
The field called 'dog' is a 1-byte integer
The field called 'haircut' is a 4-byte Boolean
The field called 'representative' is a 4-byte Boolean
The field called 'authority' is a 1-byte Boolean
The field called 'plot' is a 2-byte unsigned integer
The field called 'pig' is a 4-byte floating point number
The field called 'basket' is an 8-byte floating point number
The field called 'laborer' is a 4-byte integer
The field called 'net' is an 8-byte floating point number
The field called 'parent' is a fixed-length string of up to 7 characters (including one or more nulls)
