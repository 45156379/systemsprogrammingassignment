#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef int bool;
#define true 1
#define false 0

typedef struct {
        char decision;
        char snake;
        short caption;
        int grip;
        unsigned char support;
        long apple;
        char dog;
        int haircut;
        int representative;
        char authority;
        unsigned short plot;
        float pig;
        double basket;
        int laborer;
        double net;
        char parent[7];
} myStruct;



int main (int argc, char **argv) {
        FILE* inputFile;
        char Char;
        short Short;
        int Int;
        unsigned char UChar;
        long Long;
        unsigned short UShort;
        float Float;
        double Double;
        char parent[7];
        bool Bool;

        myStruct Struct;

        if (argc < 2){
                fprintf(stderr, "Please enter a filename as an argument!\n");
                exit(2);
        }

        if (argc > 1){                                                                                                                                                                    
                inputFile = fopen( argv[1], "rb" );                                                                                                                                       
                if ( inputFile == NULL ) {                                                                                                                                                
                        fprintf(stderr, "Unable to open %s\n", argv[1]);                                                                                                                  
                        exit(1);                                                                                                                                                          
                }                                                                                                                                                                         
                printf("decision, snake, caption, grip, support, apple, dog, haircut, representative, authority, plot, pig, basket, laborer, net, parent\n");                             
                while(!feof(inputFile)) {                                                                                                                                                 
                                                                                                                                                                                          
                                                                                                                                                                                          
                        if(fread(&Struct.decision, sizeof(Struct.decision), 1, inputFile)!= 0) {                                                                                          
                                printf("%hhd,", Struct.decision);                                                                                                                         
                        }                                                                                                                                                                 
                        else{                                                                                                                                                             
                                break;                                                                                                                                                    
                        }                                                                                                                                                                 
                        fread(&Struct.snake, sizeof(Struct.snake), 1, inputFile);                                                                                                         
                        printf(" %c,", Struct.snake);                                                                                                                                     
                        fread(&Struct.caption, sizeof(Struct.caption), 1, inputFile);                                                                                                     
                        printf(" %hd,", Struct.caption);                                                                                                                                  
                        fread(&Struct.grip, sizeof(Struct.grip), 1, inputFile);                                                                                                           
                        printf(" %hd,", Struct.grip);                                                                                                                                     
                        fread(&Struct.support, sizeof(Struct.support), 1, inputFile);                                                                                                     
                        printf(" %hhu,", Struct.support);                                                                                                                                 
                        fread(&Struct.apple, sizeof(Struct.apple), 1, inputFile);                                                                                                         
                        printf(" %ld,", Struct.apple);                                                                                                                                    
                        fread(&Struct.dog, sizeof(Struct.dog), 1, inputFile);                                                                                                             
                        printf(" %hhx,", Struct.dog);                                                                                                                                     
                        fread(&Struct.haircut, sizeof(Struct.haircut), 1, inputFile);                                                                                                     
                        printf(" %d,", Struct.haircut);                                                                                                                                   
                        fread(&Struct.representative, sizeof(Struct.representative), 1, inputFile);                                                                                       
                        printf(" %d,", Struct.representative);                                                                                                                            
                        fread(&Struct.authority, sizeof(Struct.authority), 1, inputFile);                                                                                                 
                        printf(" %hhd,", Struct.authority);                                                                                                                               
                        fread(&Struct.plot, sizeof(Struct.plot), 1, inputFile);                                                                                                           
                        printf(" %hu,", Struct.plot);                                                                                                                                     
                        fread(&Struct.pig, sizeof(Struct.pig), 1, inputFile);                                                                                                             
                        printf(" %f,", Struct.pig);                                                                                                                                       
                        fread(&Struct.basket, sizeof(Struct.basket), 1, inputFile);                                                                                                       
                        printf(" %lf,", Struct.basket);                                                                                                                                   
                        fread(&Struct.laborer, sizeof(Struct.laborer), 1, inputFile);                                                                                                     
                        printf(" %d,", Struct.laborer);                                                                                                                                   
                        fread(&Struct.net, sizeof(Struct.net), 1, inputFile);                                                                                                             
                        printf(" %lf,", Struct.net);                                                                                                                                      
                        fread(&Struct.parent, sizeof(Struct.parent), 1, inputFile);                                                                                                       
                        printf(" %s\n", Struct.parent);                                                                                                                                   
                }                                                                                                                                                                         
                fclose(inputFile);                                                                                                                                                        
        }                                                                                                                                                                                 
        return 0;                                                                                                                                                                         
}          
